package com.example.playground.repositories;

import com.example.playground.models.Employee;
import org.springframework.data.repository.CrudRepository;

/**
 * @author aashiks on 01/02/2021
 */
public interface EmployeeRepository extends CrudRepository<Employee, Long> {


}
