package com.example.playground.models;


import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

@Table("products")

@Getter
@Setter
public class Product {

    @Id
    private Long productId;
    private String productName;
    private Long supplierId;
    private Long categoryId;
    private String quantityPerUnit;
    private double unitPrice;
    private Long unitsInStock;
    private Long unitsOnOrder;
    private Long reorderLevel;
    private Long discontinued;

}
