package com.example.playground.models;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.relational.core.mapping.Table;

@Table("customer_demographics")

@Getter
@Setter
public class CustomerDemographics {

    private String customerTypeId;
    private String customerDesc;

}
