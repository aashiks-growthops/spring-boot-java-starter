package com.example.playground.models;


import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

@Table("categories")

@Getter
@Setter
public class Categories {

    @Id
    private Long categoryId;
    private String categoryName;
    private String description;
    private String picture;

}
