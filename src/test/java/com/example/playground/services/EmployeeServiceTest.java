package com.example.playground.services;

import com.example.playground.AbstractTest;
import com.example.playground.models.Employee;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author aashiks on 01/02/2021
 */
@SpringBootTest
class EmployeeServiceTest extends AbstractTest {

    @Autowired
    EmployeeService employeeService;

    @Test
    void should_findEmployeeById() {
        Optional<Employee> employeeOpt = employeeService.findEmployeeById(2L);
        assertTrue(employeeOpt.isPresent());
        Employee employee = employeeOpt.get();
        assertEquals("Andrew",employee.getFirstName());
    }
}